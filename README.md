Modern HTML and CSS Basic Examples
==================================

[![image](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://fsfe.org/campaigns/gplv3/gplv3.es.html)
[![image](https://img.shields.io/badge/language-CSS-blue.svg)](https://www.w3schools.com/css/)
[![image](https://img.shields.io/badge/language-HTML5-orange.svg)](https://www.w3schools.com/html/default.asp)

Hello everyone in this repository you will find basic examples about how to use HTML, CSS and FLEXBOX.

Topics
-------
1. HTML basic
2. Selectors
3. Fonts
4. Colors
5. Backgrounds & Borders
6. Box Model / Margin & Padding
7. Float & Align
8. Links & Buttons
9. Menu Styling
10. Inline & Block Styling
11. CSS Positioning
12. Form Styling
13. Media Queries 
14. Em & Rem Units
15. Vh & Vw Units
16. Flex Basics
17. Flex Align



**Feel free to download, I hope this repository is to your liking.**
